<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Message
 * @package App
 *
 * @property integer $author_id
 * @property integer $target_id
 * @property string $body
 */
class Message extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'author_id',
        'target_id',
        'body'
    ];

    public function author()
    {
        return $this->belongsTo('App\User', 'author_id');
    }

    public function target()
    {
        return $this->belongsTo('App\User', 'target_id');
    }
}
