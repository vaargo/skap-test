<?php

namespace App\Http\Controllers;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use App\Http\Requests\MessageValidator;
use App\User;
use App\Message;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class MessageController extends Controller
{
    /**
     * @param Request $request
     * @param $userName
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|string
     */
    public function chat(Request $request, $userName)
    {
        $users  = User::query()->select('name', 'email')->get();
        $target = User::query()->where('name', $userName)->first();

        if ($target) {
            $messages = Message::with(['author:id,name', 'target:id,name'])->where([
                'author_id' => Auth::id(),
                'target_id' => $target->id
            ])->orWhere(function (Builder $query) use ($target) {
                $query->where([
                    'author_id' => $target->id,
                    'target_id' => Auth::id()
                ]);
            })->get();

            return view('chat', [
                'alert'    => Session::get('alert', false),
                'users'    => $users,
                'target'   => $target,
                'messages' => $messages
            ]);
        } else {
            return 'Не найден адресат';
        }
    }

    /**
     * @param MessageValidator $request
     * @param string $userName
     * @return \Illuminate\Http\RedirectResponse
     */
    public function addMessage(MessageValidator $request, $userName)
    {
        $target = User::query()->where('name', $userName)->first();

        if ($target) {
            $message            = new Message();
            $message->body      = $request->input('msg');
            $message->author_id = Auth::id();
            $message->target_id = $target->id;
            $message->save();

            return redirect()->back()->with('alert', 'Ваше зачетное сообщение добавлено! А Вы молодец');
        } else {
            return redirect()->back()->with('alert', 'Получатель сообщения не найден');
        }
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function contactList()
    {
        $users = User::query()->select('name', 'email', 'created_at')->get();

        return view('list', ['users' => $users]);
    }
}
