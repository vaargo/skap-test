<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'Ошибка авторизации, пара логин/пароль не найдена',
    'throttle' => 'Воу воу! Полегче! отдохни перед тем как снова брутфорсить наш чатик!))',
    'fieldEmail' => 'E-Mail Адрес',
    'fieldPassword' => 'Пароль',
    'rememberMe'    => 'Помнить меня и любить!',
    'buttonLogin' => 'Войти',
    'foggotPassword' => 'Чет не припомню пароль...',
    'Login' => 'Авторизация',
    'Register' => 'Регистрация',

];
