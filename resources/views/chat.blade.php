@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-md-4 p-4 align-right">
        <h4>Контакты:</h4>
        <div class="btn-group-vertical" role="group">
            @forelse($users as $user)
                <a class="btn btn-dark" href="{{ route('chat',['user'=> $user->name]) }}">{{ $user->name }}</a>
            @empty
                <b>А пользователей то нет. Это конец нашей социалочке(</b>
            @endforelse
        </div>
    </div>
    <div class="col-md-8 chat messages p-4">
        <h2> Чат с {{ $target->name }} ({{$target->email}}) </h2>
        @forelse($messages as $message)
          <div class="message p-3 my-2 {{ ($message->target->id == $target->id) ? 'my' :'' }} ">
                <b>{{ $message->author->name }}:</b><br>
                {{ $message->body }}
                <span class="message-date">{{ $message->created_at->format('d.m.Y H:i:s') }}</span>
            </div>
        @empty
              <p>Нет сообщений</p>
        @endforelse
        <hr>
        @if($alert)
        <div class="alert alert-success" role="alert">
            {{ $alert }}
        </div>
        @endif
        @if($errors->has('msg'))
        <div class="alert alert-danger" role="alert">
            {{ $errors->first('msg') }}
        </div>
        @endif
        <form method="POST">
            @csrf
        <div class="new-message row">
            <div class="col-md-9 p-3">
                <textarea class="p-3" name="msg" placeholder="Текст сообщения"></textarea>
            </div>
            <div class="col-md-3 p-3">
                <input class="btn" type="submit" value="Отправить">
            <div>
        </div>
        </form>
    </div>
</div>
@endsection
