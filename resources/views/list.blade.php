@extends('layouts.app')

@section('content')
<h4>Контакты:</h4>
<div class="row">
            @forelse($users as $user)
            <div class="col-md-6 p-3">
                <div class="card p-3">
                    <div class="row no-gutters">
                        <div class="col-md-4">
                            <img src="https://robohash.org/{{$user->name}}" class="card-img">
                        </div>
                        <div class="col-md-8">
                            <div class="card-body">
                                <h3 class="card-title"><a href="{{ route('chat',['user'=> $user->name]) }}">{{$user->name}}</a></h3>
                                <ul>
                                    <li><b>Имя:</b> {{ $user->name }}</li>
                                    <li><b>Дата регистрации:</b> {{ date('d.m.Y H:i:s',strtotime($user->created_at))}}</li> 
                                    <li><b>Email:</b> {{$user->email}}</li>
                                 </ul>
                             </div>
                         </div>
                     </div> 
                 </div>
             </div>
            @empty
                <b>А пользователей то нет. Это конец нашей социалочке(</b>
            @endforelse
</div>
@endsection
