<?php

use Illuminate\Database\Seeder;
use App\Message;

class MessageTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->command->info('Добавление сообщений');
        DB::table('messages')->truncate();
        Message::create([
            'author_id' => 1,
            'target_id' => 3,
            'body' => 'Сообщение из простого текста 1'
        ]);
        Message::create([
            'author_id' => 3,
            'target_id' => 1,
            'body' => 'Сообщение из простого текста 2'
        ]);
        Message::create([
            'author_id' => 1,
            'target_id' => 2,
            'body' => 'Сообщение из простого текста 3'
        ]);
        Message::create([
            'author_id' => 1,
            'target_id' => 3,
            'body' => 'Сообщение из простого текста 4'
        ]);
    }
}
