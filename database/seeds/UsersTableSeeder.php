<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->command->info('Добавление пользователей');
    	DB::table('users')->truncate();
    	User::create([
    		'name' => 'Vargo',
    		'email' => 'to@7ever.ru',
    		'password' => Hash::make('1'),
    	]);
    	User::create([
    		'name' => 'test',
    		'email' => 'test@7ever.ru',
    		'password' => Hash::make('1'),
    	]);
    	User::create([
    		'name' => 'all',
    		'email' => 'all@7ever.ru',
    		'password' => Hash::make('1'),
    	]);
    	User::create([
    		'name' => 'all1',
    		'email' => 'all1@7ever.ru',
    		'password' => Hash::make('1'),
    	]);
    }
}
